import { connect } from 'react-redux'
import { translate } from 'react-i18next'
import compose from 'lodash/fp/compose'
import Container from '../base/Container'

const UserDashboard = ({ user, t }) => (
  <Container align="center">
    <h1>{`${t('welcomeUser')} ${user}`}</h1>
    Say something
  </Container>
)

const mapStateToProps = state => ({ user: state.common.user })

export default compose(
  connect(mapStateToProps),
  translate()
)(UserDashboard)
