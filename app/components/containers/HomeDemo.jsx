import { Actions } from 'jumpstate'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { translate } from 'react-i18next'
import compose from 'lodash/fp/compose'
import Container from '../base/Container'
import Button from '../base/Button'
import { Input } from '../utils/FormInputs'

const HomeDemo = ({ t, loading, handleSubmit }) => (
  <Container align="center">
    <h1>
      <i className="fa fa-user-circle" />
    </h1>
    <form onSubmit={handleSubmit}>
      <label htmlFor="user">{t('Login')}</label>
      <Field name="user" component={Input} type="text" placeholder="Username" />
      <Field name="password" component={Input} type="password" placeholder="Password" />
      <Button type="submit" title={t('Submit')} color="blue" />
    </form>
    {loading && <h1><i className="fa fa-spin fa-spinner" /></h1>}
  </Container>
)

const mapStateToProps = state => ({
  loading: state.common.loading
})

export default compose(
  reduxForm({
    form: 'authentication',
    pure: true,
    onSubmit: e => Actions.login(e)
  }),
  translate(),
  connect(mapStateToProps)
)(HomeDemo)
