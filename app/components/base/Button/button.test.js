import Button from '.'

it('renders with full props', () => {
  const props = {
    color: 'red',
    title: 'Foo',
    onClick: () => 'Bar'
  }
  const wrapper = shallow(<Button {...props} />)

  expect(toJson(wrapper)).toMatchSnapshot()

  expect(wrapper.props().onClick).toBeDefined()
  expect(wrapper.props().className).toBe('btn--red')
  expect(wrapper.props().children).toEqual('Foo')
})

it('renders without title', () => {
  const wrapper = shallow(<Button onClick={() => 'Foo'} />)
  expect(wrapper.props().children).toEqual('Title')
  expect(wrapper.props().className).toBe('btn')
})

it('should handle the click event', () => {
  window.alert = jest.fn()
  const wrapper = shallow(<Button onClick={() => alert('Fine')} />) // eslint-disable-line
  wrapper.simulate('click')
  expect(window.alert).toHaveBeenCalledWith('Fine')
})
