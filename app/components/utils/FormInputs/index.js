const Input = ({ meta: { touched, error }, input, ...rest }) => (
  <input
    {...input}
    {...rest}
    className={`form-control ${touched && error && '-hasError'}`}
  />
)

// eslint-disable-next-line
export { Input }
