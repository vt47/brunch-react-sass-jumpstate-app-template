import { Redirect, Route } from 'react-router-dom'

const renderFunc = (WrappedComponent, pathname, allow) => props => allow ? (
  <WrappedComponent {...props} />
) : (
  <Redirect
    to={{ pathname, state: { from: props.location } }}
  />
)

const ProtectedRoute = ({
  component: Component,
  allow,
  redirect,
  ...rest
}) => <Route {...rest} render={renderFunc(Component, redirect, allow)} />

export default ProtectedRoute
