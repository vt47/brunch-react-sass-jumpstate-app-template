import { connect } from 'react-redux'
import { Route, HashRouter } from 'react-router-dom'
import Main from './components/containers/Main'
import HomeDemo from './components/containers/HomeDemo'
import Header from './components/containers/Header'
import UserDashboard from './components/containers/UserDashboard'
import ProtectedRoute from './components/utils/ProtectedRoute'

const App = ({ auth }) => (
  <HashRouter basename={window.location.pathname}>
    <div>
      <Header />
      <hr />
      <Route exact path="/" component={Main} />
      <ProtectedRoute
        path="/login"
        allow={!auth}
        redirect="/home"
        component={HomeDemo}
      />
      <ProtectedRoute
        path="/home"
        allow={auth}
        redirect="/login"
        component={UserDashboard}
      />
    </div>
  </HashRouter>
)

const mapStateToProps = state => ({
  auth: state.common.auth
})

export default connect(mapStateToProps)(App)
