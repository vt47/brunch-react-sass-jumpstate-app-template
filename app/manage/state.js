// The initial state with default props
const state = {
  screenIndex: 0,
  loading: false,
  asyncProp: 'We will rock you!',
  formText: '',
  locales: 'jp',
  auth: false,
  user: ''
}

// eslint-disable-next-line
export { state } 
