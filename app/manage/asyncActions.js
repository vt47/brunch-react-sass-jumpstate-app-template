import { Effect, Actions } from 'jumpstate'

const asyncAction1 = Effect('login', values => {
  Actions.common.setLoading(true)
  setTimeout(() => {
    Actions.common.setLoading(false)
    Actions.common.authenticate(true)
    Actions.common.getCredentials(values.user)
  }, 3000)
})

export default { asyncAction1 }
