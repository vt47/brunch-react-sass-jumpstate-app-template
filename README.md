# BRUNCH + REACT + REDUX + JUMPSTATE + REACT-STYLEGUIDIST + JEST + ENZYME
***Optimal, developer-focused and stable React-App template***


#### **@vutran47**

## **Dependencies**
---
- **Brunch**: a build tool focus on simplicity, readability and ease-of-use
- **React**: Facebook's revolutionary front-end library
- **History + React-Router-Dom**: for client-side routing
- **Redux + Redux-Form + Jumpstate**: state-management made easier
- **Normalize.css**: unify browser's styling
- **React-Styleguidist**: isolated environment for React-Component documentation & design
- **Jest + Enzyme**: unit & snapshot testing
- **Lodash**: function utilities that makes writing javascript a joy

...and more to list.
&nbsp;
&nbsp;



## **Getting started**
---
1. Clone the repo and install dependencies
```bash
$ git clone <repo-link> <folder-name>
$ cd <folder-name>
$ npm install
```
2. Go to **package.json** and edit the App's name, description, author, version, repo url and license.
```javascript
"name": "your-app",
"description": "Description",
"author": "Your Name",
"version": "0.0.1",
"repository": {
  "type": "git",
  "url": ""
},
```
After that you can start hacking with the code

3. Get the app running by running the command in the terminal
```bash
$ npm start
```
4. Open your favourite browser and go to **http://localhost:3333** to enjoy

#### Happy Coding!
&nbsp;
&nbsp;


## **Where to go from here?**
---
For detail explanations and tutorials, check out the **documents** folder in the repo.
&nbsp;
&nbsp;


## **MIT LICENSE**
---
Copyright 2017 **vutran47**

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
