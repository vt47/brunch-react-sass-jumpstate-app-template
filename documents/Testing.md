#### **Key Understandings**:
- Test-Driven-Development
- Jest & Enzyme
- Unit Testing
- Snapshot Testing
- Code Coverage