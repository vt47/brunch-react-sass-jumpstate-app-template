#### **Key Understandings**:
- Props & State & Component LifeCycle
- SyntheticEvent
- Refs
- Function-binding
- Stateful vs Stateless Components
- High-Order-Component / High-Order-Function (HOC) and Decorator
- Function Compose & Lodash