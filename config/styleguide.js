/* eslint-disable */
const { createConfig, addPlugins, babel, match, css, sass } = require('webpack-blocks')
const { ProvidePlugin } = require('webpack')

module.exports = {
  components: '../app/components/base/**/index.js',
  styleguideDir: '../styleguide',
  webpackConfig: createConfig([
    addPlugins([
      new ProvidePlugin({
        React: 'react'
      })
    ]),
    babel(),
    match('*.scss', [
      sass(),
      css.modules()
    ])
  ])
}
